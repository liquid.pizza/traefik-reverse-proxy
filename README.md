# Readme

## Setup

Following the _digital ocean_ tutorial: https://www.digitalocean.com/community/tutorials/how-to-use-traefik-v2-as-a-reverse-proxy-for-docker-containers-on-ubuntu-20-04

### Modify `traefik_dynamic.toml`

#### Add basic auth middleware

First we have to generate basic auth credentials:

```sh
sudo apt-get install apache2-utils
htpasswd -nb <user> <password>
```

Output should look like this:

```sh
root@host:~/traefik# htpasswd -nb test_user test_password
test_user:$apr1$hwL99k2r$AcEmj5c505vMmcHzW3P.q.
```

Set the basic auth credentials in `traefik_dynamic.toml`. It should look like this:

```sh
[http.middlewares.simpleAuth.basicAuth]
  users = [
    "test_user:$apr1$hwL99k2r$AcEmj5c505vMmcHzW3P.q."
  ]
```

#### Set routing rule

Change the routing rule to your domain. For example:

```sh
[http.routers.api]
  rule = "Host(`traefik.<domain>`)"
```

More possible rules are documented in the traefik documentation: https://doc.traefik.io/traefik/routing/routers/#rule

### Modify `traefik.toml`

Set the mail address in

```sh
[certificatesResolvers.lets-encrypt.acme]
  email = "<user>@<domain>"
  storage = "/etc/traefik/acme.json"
  [certificatesResolvers.lets-encrypt.acme.tlsChallenge]
```

### Start the container

Before starting make sure, to create the docker network `web`:

`docker network create web`

And that the file `acme.json` has the permissions 600:

```sh
root@host:~/traefik# ll acme.json 
-rw------- 1 root root 0 Mar 21 18:55 acme.json
```

Run the `docker-compose.yml`:

```sh
docker-compose up -d
```

**The URL https://traeafik.\<domain\> should prompt a login and forward to the traefik dashboard.**

## Add a new container

To detect a container add some labels and the network `web` to the `docker-compose.yml`:

```sh
    labels:
      - traefik.http.routers.blog.rule=Host(`<subdomain>.<domain>`)
      - traefik.http.routers.blog.tls=true
      - traefik.http.routers.blog.tls.certresolver=lets-encrypt
      - traefik.port=80
    networks:
      - web

networks:
  web:
    external:
      name: web
```
